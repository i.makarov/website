


///////////////ANIMATION

const animItems = document.querySelectorAll('._anim-items');

if (animItems.length > 0) {
    window.addEventListener('scroll', animOnScroll);

    function animOnScroll() {
        for (let index = 0; index < animItems.length; index++) {
            const animItem = animItems[index];
            const animItemHeight = animItem.offsetHeight;
            const animItemOffset = offset(animItem).top;
            const animStart = 4;

            let animItemPoint = window.innerHeight - animItemHeight / animStart;

            if (animItemHeight > window.innerHeight) {
                animItemPoint = window.innerHeight - window.innerHeight / animStart;
            }

            if ((pageYOffset > animItemOffset - animItemPoint) && pageYOffset < (animItemOffset + animItemHeight)) {
                animItem.classList.add('_active');

            } else {
                // animItem.classList.remove('_active')
            }

        };
    };
    function offset(el) {
        const rect = el.getBoundingClientRect(),
            scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
            scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
    }
    setTimeout(() => {
        animOnScroll();
    }, 300)
}



//BURGER



const iconMenu = document.querySelector('.menu__icon');

if (iconMenu) {
    const menuBody = document.querySelector('.header__content');
    iconMenu.addEventListener("click", function (e) {
        document.body.classList.toggle('_lock')
        iconMenu.classList.toggle('_active');
        menuBody.classList.toggle('_active');
        // if (document.querySelector('.menu__body').classList.contains("_active")) {

        //     var picHolder = document.getElementById("menu__body");
        //     var ht1 = document.getElementById("header__right");

        //     ht1.style.display = 'block'
        //     picHolder.appendChild(ht1);
        //     // Здесь может быть любой другой ваш код

        // }
        // if (!document.querySelector('.menu__body').classList.contains("_active")) {

        //     var picHolder = document.getElementById("menu__body");
        //     var ht1 = document.getElementById("header__right");
        //     //picHolder.appendChild(ht1);
        //     picHolder.after(ht1);
        //     //ht1.style.display = 'none'

        //     // Здесь может быть любой другой ваш код

        // }
    });
};




// if (document.querySelector('.menu__body').classList.contains("_active")) {

//     var picHolder = document.getElementById("header__content");
//     var ht1 = document.getElementById("header__right");

//     ht1.style.display = 'block'
//     picHolder.appendChild(ht1);
//     // Здесь может быть любой другой ваш код

// }

// if (!document.querySelector('.menu__body').classList.contains("_active")) {


//     // ht1.style.display = 'none'
//     // ht1.appendChild(picHolder);
//     // Здесь может быть любой другой ваш код

// }
// ht1.appendChild(picHolder);
// ht1.style.display = 'none'

//SCROLL
const menuLinks = document.querySelectorAll('.menu__link[data-goto]');

if (menuLinks.length > 0) {
    menuLinks.forEach(menuLink => {
        menuLink.addEventListener("click", onMenuLinkClick);
    });
    function onMenuLinkClick(e) {
        const menuLink = e.target;
        if (menuLink.dataset.goto && document.querySelector(menuLink.dataset.goto)) {
            const gotoBlock = document.querySelector(menuLink.dataset.goto);
            const gotoBlockValue = gotoBlock.getBoundingClientRect().top;// + pageYOffset - document.querySelector('header').offsetHeight;

            window.scrollTo({
                top: gotoBlockValue,
                behavior: "smooth"
            });
            e.preventDefault();

        }
        const iconMenu = document.querySelector('.menu__icon');


        const menuBody = document.querySelector('.header__content');

        //document.body.classList.toggle('_lock')
        iconMenu.classList.toggle('_active');
        menuBody.classList.toggle('_active');
        // if (document.querySelector('.menu__body').classList.contains("_active")) {

        //     var picHolder = document.getElementById("menu__body");
        //     var ht1 = document.getElementById("header__right");

        //     ht1.style.display = 'block'
        //     picHolder.appendChild(ht1);
        //     // Здесь может быть любой другой ваш код

        // }
        // if (!document.querySelector('.menu__body').classList.contains("_active")) {

        //     var picHolder = document.getElementById("menu__body");
        //     var ht1 = document.getElementById("header__right");
        //     //picHolder.appendChild(ht1);
        //     picHolder.after(ht1);
        //     //ht1.style.display = 'none'

        //     // Здесь может быть любой другой ваш код

        // }


    }
}

////IBG
function ibg() {

    let ibg = document.querySelectorAll("._ibg");
    for (var i = 0; i < ibg.length; i++) {
        if (ibg[i].querySelector('img')) {
            ibg[i].style.backgroundImage = 'url(' + ibg[i].querySelector('img').getAttribute('src') + ')';
        }
    }
}

ibg();
